﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{
    public float speed;
    public float rotationSpeed;
    private Rigidbody2D rbd2;

    // Start is called before the first frame update
    void Start()
    {
        rbd2 = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // for movement
        GetComponent<Rigidbody2D>().
            AddForce(transform.up * speed *
                Input.GetAxis("Vertical"));

        //for rotation
        transform.Rotate(0, 0, -Input.GetAxis("Horizontal") *
            rotationSpeed * Time.deltaTime); 
     
    }
}